# ADM2
ADM2 Is the best companion for your Pyme

![screenshot](https://i.ibb.co/Hr10h27/Screenshot-1.png)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and hot-reloads for electron
```
yarn run electron:serve
```